<?php
App::uses('AppShell', 'Console/Command');
App::uses('ConnectionManager', 'Model');
App::uses('Ftp', 'Ftp.Model');
use Ifsnop\Mysqldump as IMysqldump;

class DumpShell extends AppShell {

    public function main()
    {
        $config = ConnectionManager::getDataSource('default')->config;
        $file = date('Y-m-d').'-'.$config['database'].'.sql';
        try {
            $dump = new IMysqldump\Mysqldump("mysql:host={$config['host']};port={$config['port']};dbname={$config['database']}", $config['login'], $config['password']);
            $dump->start(TMP.$file);
        } catch (\Exception $e) {
            $this->log('MysqlDump: '.$e->getMessage(), 'DumpSql');
            return $this->out('MysqlDump: '.$e->getMessage());
        }

        try {
            $Ftp = new Ftp();
            $Ftp->connect(Configure::read('Ftp.login'));
        } catch (Exception $e) {
            $this->log('Ftp: Impossible de se connecter au ftp', 'DumpSql');
            return $this->out('<error>Impossible de se connecter au ftp</error>');
        }

        try {
            if ($Ftp->save([
                'local' => TMP.$file,
                'remote' => Configure::read('Ftp.directory').$file,
            ])) {
                $this->out('upload reussi');
            }
        } catch (Exception $e) {
            $this->log('Ftp: '.$e->getMessage(), 'DumpSql');
            $this->out('Ftp: '.$e->getMessage());
        }
    }
}