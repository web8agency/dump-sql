# DumpSql v1.0.2

DumpSql is a plugin for CakePHP

* [ifsnop/mysqldump-php](https://github.com/ifsnop/mysqldump-php)
* [fotografde/cakephp-ftp](https://github.com/fotografde/cakephp-ftp)

## Requirements

* CakePHP >= 2.3

## Installation

Ensure require is present in composer.json. This will install the plugin into Plugin/DumpSql:

	{
		"require": {
			"web8agency/cake-helper": "*"
		}
	}

### Enable plugin

You need to enable the plugin in your app/Config/bootstrap.php file:

`CakePlugin::load('DumpSql');`

If you are already using `CakePlugin::loadAll();`, then this is not necessary.


## Usage

To configure your remote ftp access, add to your bootstrap.php:

	Configure::write('Ftp.login', [
	    'host' => 'your_host',
	    'username' => 'your-username',
	    'password' => 'your-pwd',
	    'type' => 'ftp',
	]);

To change the remote folder, add to your bootstrap.php:
	
	Configure::write('Ftp.directory', 'path/to/your/folder');