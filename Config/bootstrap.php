<?php
if(!Configure::read('Ftp.login')) trigger_error('Ftp.login is not defined');

CakeLog::config('DumpSql', array(
    'engine' => 'FileLog',
    'types' => array('DumpSql'),
    'file' => 'DumpSql',
));

Configure::write('Ftp.enabled', true);
/*Configure::write('Ftp.login', [
    'host' => 'host',
    'username' => 'username',
    'password' => 'password',
    'type' => 'ftp',
]);*/
Configure::write('Ftp.directory', './');